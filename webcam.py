import cv2

vid = cv2.VideoCapture(0) # you might need to play with this a little
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml') # also can be found in /python/site-packages/cv2/data

while True:
    ret, frame = vid.read()
    # frame = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY) # Grayscale filter
    faces_rect = face_cascade.detectMultiScale(frame, minNeighbors=5)
    for (x,y,w,h) in faces_rect:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == 27:
        break
vid.release()
cv2.destroyAllWindows()
